package com.example.nilai

abstract class Value(value : Int){
    abstract fun Count(): Float
}

class persensi(value: Int) : Value(value){
    val persentation : Float = 0.1F
    var value : Int = value
    override fun Count(): Float {
        var resultPersensi = value * persentation
        return resultPersensi
    }
}

class tugas(value: Int) : Value(value){
    val persentation : Float = 0.2F
    var value : Int = value
    override fun Count(): Float {
        var resultTugas = value * persentation
        return resultTugas
    }
}

class uts(value: Int) : Value(value){
    val persentation : Float = 0.3F
    var value : Int = value
    override fun Count(): Float {
        var resultUts = value * persentation
        return resultUts
    }
}

class uas(value: Int) : Value(value){
    val persentation : Float = 0.4F
    var value : Int = value
    override fun Count(): Float {
        var resultUas = value * persentation
        return resultUas
    }
}

class Nilai() : Mahasiswa(){
    override fun CountValue(persensi: Int, tugas: Int, uts: Int, uas: Int): Int {
        var persensi = persensi(persensi)
        var tugas = tugas(tugas)
        var uts = uts(uts)
        var uas = uas(uas)

        val resultCountValue = persensi.Count() + tugas.Count() + uts.Count() + uas.Count()
        return resultCountValue.toInt()
    }

    override fun GradeValue(resultCountValue: Int): String {
        val hasil : Int = resultCountValue
        var grade : String = ""
        if(hasil >= 80 && hasil <= 100){
            grade = "A"
        } else if(hasil >= 70 && hasil < 80){
            grade = "B"
        } else if(hasil >= 60 && hasil < 70){
            grade = "C"
        } else if(hasil >= 50 && hasil < 60){
            grade = "D"
        } else if(hasil < 50){
            grade = "E"
        }
        return grade
    }

}

fun main(){
    var persensi = Nilai()
    var nilaiakhir = persensi.CountValue(80, 80, 80, 80)
    println(nilaiakhir)
    println(persensi.GradeValue(nilaiakhir))
}