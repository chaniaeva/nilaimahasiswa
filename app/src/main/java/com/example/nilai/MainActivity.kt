package com.example.nilai

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val ipersensi = findViewById<com.google.android.material.textfield.TextInputEditText>(R.id.edtxPersensi)
        val itugas = findViewById<com.google.android.material.textfield.TextInputEditText>(R.id.edtxTugas)
        val iuts = findViewById<com.google.android.material.textfield.TextInputEditText>(R.id.edtxUts)
        val iuas = findViewById<com.google.android.material.textfield.TextInputEditText>(R.id.edtxUas)
        val btnsubmit = findViewById<Button>(R.id.ButtonSubmit)
        val textNilai = findViewById<TextView>(R.id.hasil)
        val textGrade = findViewById<TextView>(R.id.hasilGrade)

        btnsubmit.setOnClickListener {
            var isEmptyField = false
            var persensi = ipersensi.text.toString()
            var tugas = itugas.text.toString()
            var uts = iuts.text.toString()
            var uas = iuas.text.toString()
            if (persensi.isEmpty()){
                isEmptyField = true
                ipersensi.error = "Silahkan masukkan nilai absen"
            }else if (persensi.toInt() > 100 || persensi.toInt() < 0){
                isEmptyField = true
                ipersensi.error = "Range harus diatas 0 hingga 100"
        }
            if (tugas.isEmpty()){
                isEmptyField = true
                itugas.error = "Silahkan masukkan nilai tugas"
            }else if (tugas.toInt() > 100 || tugas.toInt() < 0) {
                isEmptyField = true
                itugas.error = "Range harus diatas 0 hingga 100"
            }
            if (uts.isEmpty()){
                isEmptyField = true
                iuts.error = "Silahkan masukkan nilai uts"
            }else if (uts.toInt() > 100 || uts.toInt() < 0) {
                isEmptyField = true
                iuts.error = "Range harus diatas 0 hingga 100"
            }
            if (uas.isEmpty()){
                isEmptyField = true
                iuas.error = "Silahkan masukkan nilai uas"
            }else if (uas.toInt() > 100 || uas.toInt() < 0) {
                isEmptyField = true
                iuas.error = "Range harus diatas 0 hingga 100"
            }
            if (!isEmptyField) {
                val hasil = Nilai()
                textNilai.text = hasil.CountValue(
                    ipersensi.text.toString().toInt(),
                    itugas.text.toString().toInt(),
                    iuts.text.toString().toInt(),
                    iuas.text.toString().toInt()
                ).toString()
                textGrade.text = hasil.GradeValue(textNilai.text.toString().toInt()).toString()
            }
        }
    }
}
